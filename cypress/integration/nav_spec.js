/// <reference types="Cypress" />

describe('Navigation', function() {
    it('Check titles', function() {
      cy.visit("http://localhost:3000")
      cy.title().should('eq', 'Weisswurster - Next')
      cy.get('a[data-cy="navStats"]').click()
      cy.title().should('eq', 'Weisswurster - Stats')
      cy.get('a[data-cy="navNext"]').click()
      cy.title().should('eq', 'Weisswurster - Next')
    })
  })