const puppeteer = require('puppeteer');

describe('Navigation', () => {

    var browser, page;
    beforeEach (async () => {
        browser = await puppeteer.launch({ headless: false , slowMo: 100});
        page = await browser.newPage();
        await page.setViewport({
            width: 1200,
            height: 1000
        })
      })

      afterEach (() => {
        return browser.close()
      })

      test('titles', async () => {
        await page.goto('http://localhost:3000');
        expect(await page.title()).toBe("Weisswurster - Next");
        
        await (await page.$('a[data-cy="navStats"]')).click();
        expect(await page.title()).toBe("Weisswurster - Stats");

        await (await page.$('a[data-cy="navNext"]')).click();
        expect(await page.title()).toBe("Weisswurster - Next");
    });

})