
const assert = require('assert');
const { Browser, By, Key, until } = require('selenium-webdriver');
const { ignore, suite } = require('selenium-webdriver/testing');
suite(function (env) {
  describe('Navigation', function () {
    let driver;
    before(async function () {
      driver = await env.builder().build();
    });

    it('titles', async function () {
      await driver.get('http://localhost:3000');
      await driver.wait(until.titleIs('Weisswurster - Next'), 1000);
      await driver.findElement(By.css('a[data-cy="navStats"]')).click();
      await driver.wait(until.titleIs('Weisswurster - Stats'), 1000);
      await driver.findElement(By.css('a[data-cy="navNext"]')).click();
      await driver.wait(until.titleIs('Weisswurster - Next'), 1000);
    });

    after(() => driver && driver.quit());
  })
})

