import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import NavigationTabs from './components/NavigationTabs'
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';

import { HashRouter as Router, Route } from "react-router-dom";
import { Container } from '@material-ui/core';
import NextService from './components/NextService'
import Stats from './components/Stats'
import './App.css';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    title: {
      flexGrow: 1,
    },
  }),
);


const App: React.FC = () => {
  const classes = useStyles();

  return (
    <Router>
      <div className={classes.root}>
        <AppBar position="sticky">

          <Toolbar>
            <IconButton edge="start" color="inherit" aria-label="Menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Weißwurster
            </Typography>
            <NavigationTabs></NavigationTabs>

            <IconButton
              aria-label="Account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
            >
              <AccountCircle /></IconButton>

          </Toolbar>
          {/* <NavigationTabs></NavigationTabs> */}

          {/* <Toolbar>
            <IconButton
              aria-label="Account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
            >
              <AccountCircle /></IconButton>
          </Toolbar> */}
        </AppBar>
        <Container>
          <Route path="/" exact component={NextService} />
          <Route path="/stats" exact component={Stats} />
        </Container>
      </div>
    </Router>

  );
}

export default App;
