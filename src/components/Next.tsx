import React, { useEffect } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Participants from './Participants'
import {  Element, scroller } from 'react-scroll'
import { ListItemSecondaryAction } from '@material-ui/core';
import {  NextFruhstuck } from './Entities'
import ParticipationService from "./ParticipationService"
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      padding: theme.spacing(2),
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
    },
  }),
);

export default function NextFruhstuckView(next: NextFruhstuck) {

  useEffect(() => {
    document.title = "Weisswurster - Next"
  }, [])

  const classes = useStyles();
  const orders = next.orders;
  const when = next.when;
  const theme = useTheme();
  const lg = useMediaQuery(theme.breakpoints.up('lg'));

  // function componentDidMount() {

  //   Events.scrollEvent.register('begin', function (to, element) {
  //     console.log("begin", arguments);
  //   });

  //   Events.scrollEvent.register('end', function (to, element) {
  //     console.log("end", arguments);
  //   });

  //   scrollSpy.update();
  // }

  // function componentWillUnmount() {
  //   Events.scrollEvent.remove('begin');
  //   Events.scrollEvent.remove('end');
  // }



  function handlePeopleClick(event: React.MouseEvent) {
    scroller.scrollTo("participants", {
      smooth: true,
    })
  }

  function totals() {
    return orders.reduce((a, b) => {
      return { name: "Total", brezeln: b.brezeln + a.brezeln, weisswursts: b.weisswursts + a.weisswursts };
    })
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item md={7} sm={12}>
          <ParticipationService going={true} next={next} weisswursts={2} brezeln={1} name="Me" />
        </Grid>
        <Grid item md={5} sm={12}>
          <Paper className={classes.paper}>
            <List component="nav" className={classes.root} aria-label="Mailbox folders">
              <ListItem  >
                <ListItemText primary="When" secondary={lg ? "Time of next Fruhstuck" : null} />
                <ListItemSecondaryAction>{when.format("LLLL")}</ListItemSecondaryAction>
              </ListItem>
              {/* <Divider /> */}
              <ListItem button>
                <ListItemText
                  onClick={handlePeopleClick}
                  primary="People"
                  secondary="How many people would participate" />
                <ListItemSecondaryAction>{orders.length}</ListItemSecondaryAction>
              </ListItem>
              <ListItem  >
                <ListItemText primary="Weisswursts" secondary="How many weisswursts would be needed" />
                <ListItemSecondaryAction>{totals().weisswursts}</ListItemSecondaryAction>
              </ListItem>
              {/* <Divider light /> */}
              <ListItem >
                <ListItemText primary="Brezeln" secondary="How many brezeln would be needed" />
                <ListItemSecondaryAction>{totals().brezeln}</ListItemSecondaryAction>
              </ListItem>
            </List>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Element name="participants">
            <Participants rows={orders} />
          </Element>
        </Grid>
      </Grid>
    </div>
  );
}