import React, { Component } from "react";
import { FormControlLabel, CardMedia, Theme } from "@material-ui/core";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Switch from '@material-ui/core/Switch';
import { Participation } from "./Entities"
import ParticipationDetails from "./ParticipationDetails"
import withStyles from '@material-ui/core/styles/withStyles';
import createStyles from '@material-ui/core/styles/createStyles';
import { WithStyles } from '@material-ui/core';

const styles = (theme: Theme) => createStyles(
    createStyles({
        media: {
            height: 170,
            "background-position": "0% 25%"
        },
    }),
);

type ParticipationState = Participation

type StyledProps = WithStyles & Participation

class ParticipationService extends Component<StyledProps, ParticipationState> {
    constructor(props: StyledProps) {
        super(props)

        this.state = {
            going: props.going,
            next: props.next,
            weisswursts: props.weisswursts,
            brezeln: props.brezeln,
            name: props.name
        }
    }

    handleGoing = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ ...this.state, going: event.target.checked });
    }

    render() {
        return <Card >
            <CardMedia
                className={this.props.classes.media}
                image="https://upload.wikimedia.org/wikipedia/commons/7/71/Wei%C3%9Fwurst-1.jpg"
                title="Weisswurst Fruhstuck"
            />
            <CardContent>
                <Typography variant="h6" component="h6">
                    Next Weisswurst Fruhstuck is <strong>{this.state.next.when.fromNow()} </strong>
                    <FormControlLabel
                        control={<Switch
                            checked={this.state.going}
                            onChange={this.handleGoing}
                            inputProps={{ 'aria-label': 'secondary checkbox' }}
                        />}
                        label="Are you going?"
                    />
                </Typography>


                <ParticipationDetails {...this.state} />

            </CardContent>
            <CardActions>
                <Button size="small" href="https://bar.wikipedia.org/wiki/Wei%C3%9Fwuascht">Learn More...</Button>
            </CardActions>
        </Card>
    }
}

export default withStyles(styles)(ParticipationService);