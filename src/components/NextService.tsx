import React, { Component } from "react";
import Next from './Next'
import { ParticipantOrder } from './Entities'
import moment from 'moment'

const orders = [
    createData('John Doe', 2, 1),
    createData('King', 5, 2),
    createData('Alice', 2, 1),
    createData('Mad Hatter', 1, 3),
    createData('Sleepy Dormouse', 2, 1),
    createData('March Hare', 3, 1),
    createData('Queen of Hearts', 2, 1),
];

function createData(
    name: string,
    weisswursts: number,
    brezeln: number,
): ParticipantOrder {
    return { name, weisswursts, brezeln };
}

class NextService extends Component {
    render() {
        return <Next orders={orders} when={this.nextFriday()} />
    }
    nextFriday() {
        const dayINeed = 5;
        const today = moment().isoWeekday();
        if (today <= dayINeed) {
            return moment().isoWeekday(dayINeed);
        } else {
            return moment().add(1, 'weeks').isoWeekday(dayINeed);
        }
    }
}

export default NextService;