import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import TestingMarkers from "./TestingMarkers"
import NextWeekIcon from '@material-ui/icons/NextWeek';
import BarChartIcon from '@material-ui/icons/BarChart';

interface TabContainerProps {
    children?: React.ReactNode;
}

function TabContainer(props: TabContainerProps) {
    return (
        <Typography component="div" >
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        fullHeight: {
            ...theme.mixins.toolbar,
        },
    }),
);

export default function SimpleTabs() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    function handleChange(event: React.ChangeEvent<{}>, newValue: number) {
        setValue(newValue);
    }
    return (
        <div className={classes.root}>
            <Tabs
                classes={{ root: classes.fullHeight }}
                value={value}
                onChange={handleChange}>
                <Tab
                    icon={<NextWeekIcon />}
                    classes={{ root: classes.fullHeight }}
                    label="Next"
                    component={Link}
                    to="/"
                    {...TestingMarkers.navNext}
                />
                <Tab
                    classes={{ root: classes.fullHeight }}
                    label="Stats"
                    icon={<BarChartIcon />}
                    component={Link}
                    to="/stats"
                    {...TestingMarkers.navStats} />
            </Tabs>
        </div>
    );
}