import React, { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import { Participation } from './Entities'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
            maxWidth: 300,
        },
    }),
);

const weisswurstsOptions = [
    1,
    2,
    3,
    4,
    5
];

const bretzelnOptions = [
    1,
    2,
    3
];


export default function ParticipationDetailsControl(props: Participation) {
    const classes = useStyles();
    // const theme = useTheme();
    const [participation, setParticipation] = React.useState<Participation>(props);

    function handleWeisswursts(event: React.ChangeEvent<{ value: unknown }>) {
        setParticipation({ ...participation, weisswursts: event.target.value as number });
    }

    function handleBretzeln(event: React.ChangeEvent<{ value: unknown }>) {
        setParticipation({ ...participation, brezeln: event.target.value as number });
    }

    useEffect(() => {
        setParticipation(p => { return { ...p, going: props.going } });
    }, [props.going]);

    return (
        !participation.going ? <div></div>
            : <div className={classes.root}>

                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="select-multiple">Weisswursts</InputLabel>
                    <Select
                        value={participation.weisswursts}
                        onChange={handleWeisswursts}
                        input={<Input id="select-multiple" />}
                    >
                        {weisswurstsOptions.map(opt => (
                            <MenuItem key={opt} value={opt}>
                                {opt}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>

                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="select-multiple">Bretzeln</InputLabel>
                    <Select
                        value={participation.brezeln}
                        onChange={handleBretzeln}
                        input={<Input id="select-multiple" />}
                    >
                        {bretzelnOptions.map(opt => (
                            <MenuItem key={opt} value={opt}>
                                {opt}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>

            </div>
    );
}
