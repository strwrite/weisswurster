import moment from "moment"

export interface ParticipantOrder {
    weisswursts: number;
    brezeln: number;
    name: string;
}

export interface NextFruhstuck {
    orders: ParticipantOrder[]
    when: moment.Moment
}

export type Participation = ParticipantOrder & {
    next: NextFruhstuck
    going: boolean
} 
