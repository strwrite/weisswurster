import React, { useEffect } from 'react';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import OrdersChart from './OrdersChart'
import PeopleChart from './PeopleChart'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '90%',
      padding: theme.spacing(2),
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    icon: {
      verticalAlign: 'bottom',
      height: 20,
      width: 20,
    },
    details: {
      alignItems: 'center',
    },
    column: {
      flexBasis: '33.33%',
    },
    helper: {
      borderLeft: `2px solid ${theme.palette.divider}`,
      padding: theme.spacing(1, 2),
    },
    link: {
      color: theme.palette.primary.main,
      textDecoration: 'none',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
  }),
);

const orders = [
  {
    name: 'Page A', weisswursts: 4000, brezeln: 2400
  },
  {
    name: 'Page B', weisswursts: 3000, brezeln: 1398
  },
  {
    name: 'Page C', weisswursts: 2000, brezeln: 9800
  },
  {
    name: 'Page D', weisswursts: 2780, brezeln: 3908
  },
  {
    name: 'Page E', weisswursts: 1890, brezeln: 4800
  },
  {
    name: 'Page F', weisswursts: 2390, brezeln: 3800
  },
  {
    name: 'Page G', weisswursts: 3490, brezeln: 4300
  },
];

export default function DetailedExpansionPanel() {
  const classes = useStyles();

  useEffect(() => {
    document.title = "Weisswurster - Stats"
  }, [])

  return (
    <div className={classes.root}>
      <ExpansionPanel defaultExpanded>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          <div className={classes.column}>
            <Typography className={classes.heading}>Orders</Typography>
          </div>
          <div className={classes.column}>
            <Typography className={classes.secondaryHeading}>Chart, that displays number of weisswursts and brezeln ordered in last few breakfasts</Typography>
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.details}>
          <OrdersChart data={orders}/>
        </ExpansionPanelDetails>
        <Divider />
        <ExpansionPanelActions>
          {/* <Button size="small">Cancel</Button>
          <Button size="small" color="primary">
            Save
          </Button> */}
        </ExpansionPanelActions>
      </ExpansionPanel>

      <ExpansionPanel defaultExpanded>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          <div className={classes.column}>
            <Typography className={classes.heading}>People</Typography>
          </div>
          <div className={classes.column}>
            <Typography className={classes.secondaryHeading}>Chart, that displays number of people attended last few breakfasts</Typography>
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.details}>
          <PeopleChart data={[
            {
              name: 'Page A', value: 4000
            },
            {
              name: 'Page B', value: 3000
            },
            {
              name: 'Page C', value: 2000
            },
            {
              name: 'Page D', value: 2780
            },
            {
              name: 'Page E', value: 1890
            },
            {
              name: 'Page F', value: 2390
            },
            {
              name: 'Page G', value: 3490
            },
          ]} />
        </ExpansionPanelDetails>
        <Divider />
        <ExpansionPanelActions>
          {/* <Button size="small">Cancel</Button>
          <Button size="small" color="primary">
            Save
          </Button> */}
        </ExpansionPanelActions>
      </ExpansionPanel>



    </div>
  );
}